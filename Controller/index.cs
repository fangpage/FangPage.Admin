﻿using System;
using System.Collections.Generic;
using System.Text;
using FangPage.Core;
using FangPage.User;
using FangPage.User.Model;
using FangPage.Common;

namespace FangPage.Admin.Controller
{
    [WebView("index.html")]
    public class index:FPController
    {
        public override void Controller()
        {
            AuthInfo authinfo = FPUser.Authorize();

            if (!authinfo.islogin)
            {
                FPResponse.Redirect("login.html");
            }
        }
    }
}
