﻿using FangPage.Admin.Model;
using FangPage.Common;
using FangPage.Core;
using FangPage.Data;
using FangPage.User;
using System;
using System.Collections.Generic;

namespace FangPage.Admin.Controller
{
    public class menu_add:AdminController
    {
        protected MenuInfo menuinfo = new MenuInfo();

        public override void Controller()
        {
            string id = FPRequest.GetString("id");

            string parentid = FPRequest.GetString("parentid");

            if (!id.IsNullOrEmpty())
            {
                menuinfo = DbHelper.ExcuteModel<MenuInfo>(id);

                parentid = menuinfo.parentid;
            }
            else
            {
                menuinfo.parentid = parentid;
            }

            if (ispost)
            {
                menuinfo = FPRequest.GetModel(menuinfo);

                if (menuinfo.id.IsNullOrEmpty())
                {
                    DbHelper.ExcuteInsert(menuinfo);

                    FPResponse.WriteSuccess("添加成功");
                }
                else
                {
                    DbHelper.ExcuteUpdate(menuinfo);

                    if(menuinfo.markup=="homeindex")
                    {
                        AppSettings.SetAppSetting(appsettings.path, "homeindex", menuinfo.url);
                    }

                    FPResponse.WriteSuccess("修改成功");
                }

                return;
            }

            if (id.IsNullOrEmpty())
            {
                List<SqlParam> where = new List<SqlParam>();

                if (parentid.IsNullOrEmpty())
                {
                    where.Add(DbHelper.MakeOrWhere("parentid", WhereType.IsNullOrEmpty));
                }
                else
                {
                    where.Add(DbHelper.MakeAndWhere("parentid", parentid));
                }

                menuinfo.display = DbHelper.ExcuteCount<MenuInfo>(where.ToArray()) + 1;
            }

            FPResponse.WriteData(menuinfo);
        }
    }
}
