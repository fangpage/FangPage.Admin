﻿using FangPage.Core;
using FangPage.User.Model;
using FangPage.User;
using System;
using System.Collections.Generic;
using System.Text;
using FangPage.Data;
using FangPage.Admin.Model;

namespace FangPage.Admin.Controller
{
    public class menu_list : AdminController
    {
        public override void Controller()
        {
            if (ispost)
            {
                string ids = FPRequest.GetString("ids");

                DbHelper.ExcuteDelete<MenuInfo>(ids);

                SqlParam del_where = DbHelper.MakeAndWhere("parentid", WhereType.In, ids);

                DbHelper.ExcuteDelete<MenuInfo>(del_where);

                FPResponse.WriteSuccess("删除成功");

                return;
            }

            SqlParam menu_sql = DbHelper.MakeOrderBy("display", OrderBy.ASC);

            List<MenuInfo> data = DbHelper.ExcuteList<MenuInfo>(menu_sql);

            FPResponse.WriteData(data);
        }
    }
}
