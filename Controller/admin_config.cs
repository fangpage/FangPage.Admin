﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using FangPage.Common;
using FangPage.Core;
using FangPage.User;
using FangPage.Data;
using FangPage.Admin.Model;

namespace FangPage.Admin.Controller
{
    public class admin_config : AdminController
    {
        public override void Controller()
        {
            if (ispost)
            {
                AppSettings.SetAppSetting(appsettings.path,"title", FPRequest.GetString("title"));

                AppSettings.SetAppSetting(appsettings.path, "copyright", FPRequest.GetString("copyright"));

                string homeindex = FPRequest.GetString("homeindex");

                AppSettings.SetAppSetting(appsettings.path, "homeindex", homeindex);

                SqlParam[] update_where = {
                    DbHelper.MakeSet("url",homeindex),
                    DbHelper.MakeAndWhere("markup", "homeindex")
                };

                DbHelper.ExcuteUpdate<MenuInfo>(update_where);

                FPResponse.WriteSuccess("保存成功。");

                return;
            }

            FPResponse.WriteData(appsettings);
        }
    }
}
