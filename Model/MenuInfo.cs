﻿using FangPage.Common;
using FangPage.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FangPage.Admin.Model
{
    public class MenuInfo
    {
        private string m_id = String.Empty;//ID
        private string m_parentid = String.Empty;//父ID
        private string m_parentname = String.Empty;//父名称
        private string m_name = String.Empty;//名称
        private string m_markup = String.Empty;//标识
        private string m_icon = String.Empty;//图标
        private string m_url = String.Empty;//地址
        private int m_depth;//层级
        private int m_display;//排序
        private bool m_status = true;//是否可用

        /// <summary>
        /// ID
        /// </summary>
        [PrimaryKey]
        [UUID]
        public string id
        {
            get { return m_id; }
            set { m_id = value; }
        }

        /// <summary>
        /// 父ID
        /// </summary>
        [LeftJoin("MenuInfo",1,"id")]
        public string parentid
        {
            get { return m_parentid; }
            set { m_parentid = value; }
        }

        /// <summary>
        /// 父名称
        /// </summary>
        [Map("MenuInfo",1, "name")]
        public string parentname
        {
            get
            {
                if (m_id.IsNullOrEmpty() && !m_parentid.IsNullOrEmpty())
                {
                    MenuInfo parentinfo = DbHelper.ExcuteModel<MenuInfo>(m_parentid);

                    m_parentname = parentinfo.name;
                }

                return m_parentname;
            }
            set { m_parentname = value; }
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        /// <summary>
        /// 标识
        /// </summary>
        public string markup
        {
            get { return m_markup; }
            set { m_markup = value; }
        }

        /// <summary>
        /// 图标
        /// </summary>
        public string icon
        {
            get { return m_icon; }
            set { m_icon = value; }
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string url
        {
            get { return m_url; }
            set { m_url = value; }
        }

        /// <summary>
        /// 层级
        /// </summary>
        public int depth
        {
            get { return m_depth; }
            set { m_depth = value; }
        }

        /// <summary>
        /// 排序
        /// </summary>
        public int display
        {
            get { return m_display; }
            set { m_display = value; }
        }

        /// <summary>
        /// 是否可用
        /// </summary>
        public bool status
        {
            get { return m_status; }
            set { m_status = value; }
        }
    }
}
